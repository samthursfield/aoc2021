using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

class Program {
    int[] GetInput(string filename) {
        using (var sr = new StreamReader(filename)) {
            var text = sr.ReadToEnd();
            List<int> result = new List<int>();
            foreach (string number in text.Split('\n', StringSplitOptions.RemoveEmptyEntries)) {
                result.Add(Int32.Parse(number));
            }
            return result.ToArray();
        }
    }

    int WindowSum(int position, int size, int[] measurements) {
        int result = 0;
        for (int i=0; i < size; i++) {
            result += measurements[position + i];
        }
        return result;
    }

    int CountIncrements(int[] measurements, int window_size) {
        int increments = 0;

        for (var position = 0; position <= measurements.Length; position ++) {
            if (position > window_size) {
                var sum = this.WindowSum(position - window_size, window_size, measurements);
                var previous_sum = this.WindowSum(position - window_size - 1, window_size, measurements);
                if (sum > previous_sum) {
                    increments ++;
                }
                Console.WriteLine(String.Format("{0}: prev {1}, now {2}: increments now {3}", position, previous_sum, sum, increments));
            }
        }

        return increments;
    }

    static void Main(string[] args) {
        var filename = "day1.input";
        if (args.Length > 0) {
            filename = args[0];
        };
        Program self = new Program();
        var measurements = self.GetInput(filename);
        var increments = self.CountIncrements(measurements, 3);
        Console.WriteLine("Increments: " + increments);
    }
}