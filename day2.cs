using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

class Program {
    string[] GetInput(string filename) {
        using (var sr = new StreamReader(filename)) {
            var text = sr.ReadToEnd();
            return text.Split('\n', StringSplitOptions.RemoveEmptyEntries);
        }
    }

    (int, int) DriveSubmarine(string[] commands) {
        int aim = 0, y = 0, depth = 0;
        foreach (var command in commands) {
            var parts = command.Split(' ');
            switch (parts[0]) {
                case "forward":
                   Console.WriteLine("forward {0} * {1}", parts[1], aim);
                   y += Int32.Parse(parts[1]);
                   depth += Int32.Parse(parts[1]) * aim;
                   break;
                case "up":
                   aim -= Int32.Parse(parts[1]);
                   break;
                case "down":
                   aim += Int32.Parse(parts[1]);
                   break;
                default:
                    throw new Exception(String.Format("Invalid command: {0}", parts[0]));
            }
        }
        return (y, depth);
    }

    static void Main(string[] args) {
        var filename = "day2.input";
        if (args.Length > 0) {
            filename = args[0];
        };

        Program self = new Program();
        var commands = self.GetInput(filename);
        var (y, depth) = self.DriveSubmarine(commands);
        Console.WriteLine(String.Format("Position now: {0}, {1}", y, depth));
        Console.WriteLine(String.Format("Puzzle answer: {0}", y * depth));
    }
}